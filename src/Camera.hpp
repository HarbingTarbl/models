#ifndef _CAMERA_H_PP_
#define _CAMERA_H_PP_

#include "Common.hpp"
#include "Property.hpp"

using glm::fquat;

class Camera
{
public:
	virtual const fmat4& GetProjection() = 0;
	virtual const fmat4& GetView() = 0;
	virtual const fmat4& GetViewProjection() = 0;

	virtual void Update() = 0;

	virtual ~Camera() {}
};


class Orientable
{
public:
	virtual fmat4 GetRotation() = 0;
	virtual fquat GetOrientation() = 0;
	virtual float GetPitch() = 0; 
	virtual float GetYaw() = 0;
	virtual float GetRoll() = 0;
};

class Orientation
	: public Orientable
{
private:
	bool pNeedsUpdate;
	short pNeedsNormal;

	fquat pOrint;
	float pRoll, pYaw, pPitch;

public:
	virtual fquat GetOrientation();
	virtual fmat4 GetRotation() { return glm::toMat4(GetOrientation()); }

	float GetPitch();
	float GetYaw();
	float GetRoll();

	Property<float> mYaw;
	Property<float> mPitch;
	Property<float> mRoll;

	Orientation();


protected:
	virtual void update();
};

class FlightCamera
	: public Camera, public Orientation
{
public:
	fmat4 mProjection;
	fmat4 mView;
	fmat4 mViewProjection;

	fvec3 mPosition;
	fvec3 mFacing;

	virtual const fmat4& GetProjection();
	virtual const fmat4& GetView();
	virtual const fmat4& GetViewProjection();

	virtual void Update();

	FlightCamera(float fov, float zmin, float zmax, float aspect);

private:
	float pZMin, pZMax, pAspect, pFov;

};

struct SimpleCamera
{
	fmat4 mProjection;
	fmat4 mView;

	fvec3 mPosition;
	fvec3 mFront;
	fvec3 mRight;


	void Translate(const float x, const float y, const float z);
	void Translate(const fvec3& t);
	void TranslateLerp(const fvec3& t, float lerp);

	void Face(const fvec3& point);
	void FaceLerp(const fvec3& point, float lerp);

	SimpleCamera();

	void Update();
};



#endif
