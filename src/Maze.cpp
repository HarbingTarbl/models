#include "Maze.hpp"

#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem/path.hpp>

#include <iostream>
#include <exception>

using std::cout;
using std::endl;
using std::exception;

using boost::filesystem::path;
using boost::filesystem::ifstream;

Maze* MazeBuilder::CreateMazeFromFile(const string& filepath)
{
	ifstream file(path(filepath), ifstream::binary);
	int tXSize = 0, tYSize = 0;
	bool tXKnown = false, tYKnown = false;
	unique_ptr<Maze> tMaze(new Maze());
	while(file.good())
	{
		switch(file.get())
		{

		case '\n':
			if(!tXKnown)
			{
				tMaze->pXSize  = tXSize;
				tXKnown = true;
			}
			else if(tXSize != tMaze->pXSize)				
			{
				throw exception("Maze width not constant");
			}

			if(tXSize != 0)
			{
				tYSize++;
			}
			tXSize = 0;
			break;

		case '#':
			tXSize++;
			tMaze->pWalls.push_back(Maze::TileType::Wall);
			break;

		case '.':
			tXSize++;
			tMaze->pWalls.push_back(Maze::TileType::Walk);
			break;

		case ' ':
			tXSize++;
			tMaze->pWalls.push_back(Maze::TileType::Floor);
			break;

		case 'e':
			tMaze->pEndPos = glm::ivec2(tXSize, tYSize);

			tXSize++;
			tMaze->pWalls.push_back(Maze::TileType::End);
			break;

		case 's':
			tMaze->pStartPos = glm::ivec2(tXSize, tYSize);
			tXSize++;
			tMaze->pWalls.push_back(Maze::TileType::Start);
			break;

		}
	}

	if(tXSize != 0)
		tYSize++;

	tMaze->pYSize = tYSize;
	tMaze->pWalls.shrink_to_fit();


	return tMaze.release();
}

ostream& operator<<(ostream& out, Maze& mz)
{
	for(auto y = 0; y < mz.GetHeight(); y++)
	{
		for(auto x = 0; x < mz.GetWidth(); x++)
		{
			switch(mz.GetWall(x, y))
			{
			case Maze::TileType::End:
				out << "E";
				break;
			case Maze::TileType::Floor:
				out << " ";
				break;
			case Maze::TileType::None:
				out << "?";
				break;
			case Maze::TileType::Start:
				out << "S";
				break;
			case Maze::TileType::Wall:
				out << "#";
				break;
			case Maze::TileType::Walk:
				cout << ".";
				break;
			}
		}
		out << endl;
	}

	return out;
}