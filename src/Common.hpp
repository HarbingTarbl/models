#ifndef _COMMON_H_PP_
#define _COMMON_H_PP_

#include <glm/glm.hpp>
#include <glm/ext.hpp>


using glm::fmat4;
using glm::fmat3;
using glm::fvec4;
using glm::fvec3;
using glm::fvec2;

const fvec3 cYUnitVector(0, 1, 0);
const fvec3 cXUnitVector(1, 0, 0);
const fvec3 cZUnitVector(0, 0, 1);


#endif