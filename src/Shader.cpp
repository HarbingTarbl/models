#include "Shader.hpp"

#include <glload/gl_2_1.h>
#include <glutil/glutil.h>
#define BOOST_LIB_VERSION 5_4

#include <boost/filesystem/path.hpp>
#include <boost/filesystem/fstream.hpp>

#include <memory>
#include <exception>
#include <iostream>

#ifdef _DEBUG
#define CheckError(...) __VA_ARGS__ { int x = glGetError(); if(x != GL_NO_ERROR) std::cout << #__VA_ARGS__ << "@" << glutil::ErrorCodeToEnumString(x) << std::endl; }
#else
#define CheckError(...) __VA_ARGS__ {}
#endif

using std::unique_ptr;
using std::runtime_error;

using boost::filesystem::ifstream;
using boost::filesystem::path;

int Shader::GetLocation(const string& name) 
{
	auto result = Shader::sCurrentShader->pUniformLocations.find(name);

	if(result == Shader::sCurrentShader->pUniformLocations.end())
		return -1;

	return result->second;
}


Shader&  Shader::Uniform(const string& name, const fmat4& v) 
{
	auto t  = Shader::GetLocation(name);
	glUniformMatrix4fv(t, 1, false, value_ptr(v));
	return *Shader::sCurrentShader;
}

Shader&  Shader::Uniform(const string& name, const fmat3& v) 
{
	glUniformMatrix3fv(Shader::GetLocation(name), 1, false, value_ptr(v));
	return *Shader::sCurrentShader;
}


Shader&  Shader::Uniform(const string& name, const fvec4& v) 
{
	glUniform4fv(Shader::GetLocation(name), 1, value_ptr(v));
	return *Shader::sCurrentShader;
}


Shader&  Shader::Uniform(const string& name, const fvec3& v) 
{
	glUniform3fv(Shader::GetLocation(name), 1, value_ptr(v));
	return *Shader::sCurrentShader;
}


Shader&  Shader::Uniform(const string& name, const fvec2& v) 
{
	glUniform2fv(Shader::GetLocation(name), 1, value_ptr(v));
	return *Shader::sCurrentShader;
}

Shader&  Shader::Uniform(const string& name, const float v) 
{
	glUniform1f(Shader::GetLocation(name), (v));
	return *Shader::sCurrentShader;
}


Shader& Shader::Uniform(const string& name, const int v)
{
	glUniform1i(Shader::GetLocation(name), (v));
	return *Shader::sCurrentShader;
}

Shader* Shader::sCurrentShader = NULL;

Shader::Shader(const unsigned program)
	: pProgramId(program)
{
	int tUniformCount, tMaxUniformLength;

	glGetProgramiv(program, GL_ACTIVE_UNIFORMS, &tUniformCount);
	glGetProgramiv(program, GL_ACTIVE_UNIFORM_MAX_LENGTH, &tMaxUniformLength);

	unique_ptr<char[]> tUniformNameBuffer(new char[tMaxUniformLength]);

	for(unsigned i = 0; i < tUniformCount; i++)
	{
		int tUniformNameLength;
		glGetActiveUniformName(program, i, tMaxUniformLength, &tUniformNameLength, tUniformNameBuffer.get());
		pUniformLocations.emplace(string(tUniformNameBuffer.get(), tUniformNameLength), glGetUniformLocation(program, tUniformNameBuffer.get()));
	}
}

Shader::~Shader()
{
	glDeleteProgram(pProgramId);
}

void Shader::Bind() 
{
	glUseProgram(pProgramId);
	sCurrentShader = this;
}


ShaderBuilder& ShaderBuilder::AddFile(const string& pathname)
{
	path tPath(pathname);
	ifstream tFile(tPath, ifstream::binary);
	if(!tFile.good())
		throw runtime_error(string("Unable to read file ").append(tPath.string()));

	int tFileSize;
	tFile.seekg(0, ifstream::end);
	tFileSize = tFile.tellg();
	tFile.seekg(0, ifstream::beg);
	tFile.clear();

	if(tFileSize == 0)
		throw runtime_error(string(tPath.string()).append(" is empty"));

	unique_ptr<char[]> tFileBuffer(new char[tFileSize + 1]);
	tFile.read(tFileBuffer.get(), tFileSize);
	tFileBuffer[tFileSize] = 0;
	if(tFileSize != tFile.gcount())
		throw runtime_error(string("File read error ").append(tPath.string()));

	unsigned tShaderId;

	path tExt = tPath.extension();
	if(tExt == ".vertex")
		tShaderId = glCreateShader(GL_VERTEX_SHADER);
	else if(tExt == ".fragment")
		tShaderId = glCreateShader(GL_FRAGMENT_SHADER);
	else
		throw runtime_error(string("Unknown file ending").append(tPath.string()));

	const char* tShaderSource = tFileBuffer.get();

	glShaderSource(tShaderId, 1, &tShaderSource, 0);
	glCompileShader(tShaderId);

	int tCompileStatus;
	glGetShaderiv(tShaderId, GL_COMPILE_STATUS, &tCompileStatus);
	if(!tCompileStatus)
	{
		int tInfoLogLength;
		glGetShaderiv(tShaderId, GL_INFO_LOG_LENGTH, &tInfoLogLength);
		tFileBuffer.reset(new char[tInfoLogLength + 1]);
		glGetShaderInfoLog(tShaderId, tInfoLogLength + 1, NULL, tFileBuffer.get());
		throw runtime_error(string("Shader compile error, ").append(tPath.string()).append("\n").append(string(tFileBuffer.get(), tInfoLogLength)));
	}

	glAttachShader(pCurrentProgram, tShaderId);
	return *this;
}

ShaderBuilder& ShaderBuilder::AddString(const string& str, const int val)
{
	unsigned tShaderId;
	tShaderId = glCreateShader(val);

	const char* tShaderSource = str.c_str();

	glShaderSource(tShaderId, 1, &tShaderSource, 0);
	glCompileShader(tShaderId);

	int tCompileStatus;
	glGetShaderiv(tShaderId, GL_COMPILE_STATUS, &tCompileStatus);
	if(!tCompileStatus)
	{
		int tInfoLogLength;
		glGetShaderiv(tShaderId, GL_INFO_LOG_LENGTH, &tInfoLogLength);
		unique_ptr<char[]> tLinkStr(new char[tInfoLogLength + 1]);
		glGetShaderInfoLog(tShaderId, tInfoLogLength + 1, NULL, tLinkStr.get());
		throw runtime_error(string(tLinkStr.get(), tInfoLogLength));
	}

	glAttachShader(pCurrentProgram, tShaderId);
	return *this;
}

ShaderBuilder& ShaderBuilder::SetAttributeLocation(const string& attrName, const int attrLocation)
{
	glBindAttribLocation(pCurrentProgram, attrLocation, attrName.c_str());
	return *this;
}

int ShaderBuilder::GetAttributeLocation(const string& attrName)
{
	return glGetAttribLocation(pCurrentProgram, attrName.c_str());
}

Shader* ShaderBuilder::Create()
{
	glLinkProgram(pCurrentProgram);
	int tLinkStatus;
	glGetProgramiv(pCurrentProgram, GL_LINK_STATUS, &tLinkStatus);
	if(!tLinkStatus)
	{
		int tLinkErrorStrSize;
		glGetProgramiv(pCurrentProgram, GL_INFO_LOG_LENGTH, &tLinkErrorStrSize);
		unique_ptr<char[]> tLinkErrorStr(new char[tLinkErrorStrSize + 1]);
		glGetProgramInfoLog(pCurrentProgram, tLinkErrorStrSize, NULL, tLinkErrorStr.get());
		throw runtime_error(string(tLinkErrorStr.get(), tLinkErrorStrSize));
	}
	unique_ptr<Shader> shader(new Shader(pCurrentProgram));
	pCurrentProgram = 0;
	Reset();
	return shader.release();
}


void ShaderBuilder::Reset()
{
	if(pCurrentProgram)
		glDeleteProgram(pCurrentProgram);

	pCurrentProgram = glCreateProgram();
}