#ifndef _MAZE_H_PP_
#define _MAZE_H_PP_

#include "Common.hpp"

#include <string>
#include <memory>
#include <tuple>
#include <iosfwd>
#include <vector>

using std::unique_ptr;
using std::string;
using std::vector;
using std::ostream;

class Maze;

class MazeBuilder
{
private:
	friend class Maze;
	unique_ptr<Maze> pMaze;

public:
	static Maze* CreateMazeFromFile(const string& file);

};

class Maze
{
public:
	enum class TileType : unsigned char
	{
		None,
		Floor,
		Wall,
		Start,
		End,
		Walk
	};
	friend class MazeBuilder;

private:
	vector<TileType> pWalls;
	int pXSize, pYSize;
	glm::ivec2 pStartPos;
	glm::ivec2 pEndPos;

	inline Maze()
		: pXSize(0), pYSize(0)
	{

	}

public:
	
	inline const glm::ivec2& GetStart() const { return pStartPos; }
	inline const glm::ivec2& GetEnd() const { return pEndPos; }
	inline const int GetWidth() const { return pXSize; }
	inline const int GetHeight() const { return pYSize; }

	inline const TileType GetWall(const int x, const int y) const
	{ 
		return (x >= 0 && x < GetWidth() && y >= 0 && y < GetHeight()) ? GetWall(x + y * GetWidth()) : TileType::None;
	}

	inline const TileType GetWall(const int index) const
	{
		return pWalls[index];
	}

	friend ostream& operator<<(ostream& out, Maze& mz);

	inline ~Maze() {}
};

#endif