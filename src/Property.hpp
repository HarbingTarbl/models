#ifndef _PROPERTY_H_PP_
#define _PROPERTY_H_PP_
#include <functional>
using std::function;

template<typename T>
class Property
{
private:
	const function<T()> pGet;
	const function<T(const T&)> pSet;

	Property<T>(const Property<T>& other)
		: pGet(other.pGet), pSet(other.pSet)
	{

	}

public:
	typedef function<T()> GetFunction;
	typedef function<T(const T&)> SetFunction;

	auto inline operator=(const T v) -> T { return pSet(); }
	auto inline operator+(const T v) -> T { return pGet() + T; }
	auto inline operator-(const T v) -> T { return pGet() - T; }
	auto inline operator*(const T v) -> T { return pGet() * T; }
	auto inline operator/(const T v) -> T { return pGet() / T; }
	auto inline operator*=(const T v) -> T { return pSet(pGet() * v); }
	auto inline operator/=(const T v) -> T { return pSet(pGet() / v); }
	auto inline operator+=(const T v) -> T { return pSet(pGet() + v); }
	auto inline operator-=(const T v) -> T { return pSet(pGet() - v); }
	inline operator T() { return pGet(); }

	explicit Property(const GetFunction gf, const SetFunction sf)
		: pGet(gf), pSet(sf)
	{

	}



};




#endif