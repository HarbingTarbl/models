#include <glload/gl_2_1.h>
#include <glload/gl_load.hpp>

#include <glutil/glutil.h>
#include <glimg/glimg.h>

#include <GLFW/glfw3.h>

#include <glmesh/glmesh.h>

#include <vector>
#include <functional>
#include <array>
#include <cctype>
#include <new>

#include "Common.hpp"
#include "Camera.hpp"
#include "Shader.hpp"
#include "Maze.hpp"
#include "Property.hpp"

#define SHADOW_SIZE 1024

#define GLSL(str) "#version 130\n" #str
#ifdef _DEBUG
#define CheckError(...) __VA_ARGS__ if(int x = glGetError()){  std::cout << #__VA_ARGS__ << "@" << glutil::ErrorCodeToEnumString(x) << std::endl; }
#else
#define CheckError(...) __VA_ARGS__ {}
#endif

using namespace std;
using glmesh::Mesh;
using glmesh::CpuDataWriter;
using glmesh::StreamBuffer;

using glm::ivec3;
static unique_ptr<glmesh::StreamBuffer> sIBuffer;
static unique_ptr<glmesh::VertexFormat> sVFT;
static unique_ptr<glmesh::VertexFormat> sQuadFormat;
static vector<GLuint> sTextures;
static int MainShadowMode = 1;

void dgGenTextures(int number, GLuint* tex)
{
	int begin = sTextures.size();
	int end = begin + number;
	sTextures.resize(end);

	glGenTextures(number, &sTextures[begin]);
	memcpy(tex, &sTextures[begin], sizeof(GLuint) * number);
}

enum class DrawModes
{
	TextureDraw,
	ConstColor,
	ShadowPass,
	DepthPass,
	Quad
};

struct RenderPipe;
struct InputState
{
	bool IsMoving;
	bool IsTurning;

	bool WasMoving;
	bool WasTurning;

	bool ToggleDebug;
	bool ToggleNoClip;
	bool ToggleDrawWalk;
	bool ToggleBigMap;
	bool ToggleDebugTexture;
	bool HoldMoveSun;

	int DebugTextureId;

	float TurnAmount;
	float MoveAmount;

	float ZoomLevel;
	float FacingAngle;



	fvec3 FacingDir;

	fvec3 SunPosition;
	fvec3 SunDirection;
};

struct Box
{
	virtual float Top() const = 0;
	virtual float Bottom() const = 0;
	virtual float Left() const = 0;
	virtual float Right() const = 0;

	virtual fvec2 Center() const
	{
		return fvec2(Left() + (Right() - Left()) / 2.0f, Top() + (Bottom() - Top()) / 2.0f);
	}

	virtual bool Intersects(const Box& other) const
	{
		return !(other.Left() >= Right() || other.Right() <= Left() || other.Top() >= Bottom() || other.Bottom() <= Top());
	}

	virtual bool Contains(const fvec2& v) const
	{
		using namespace glm;
		float left = Left(), right = Right(), top = Top(), bottom = Bottom();


		if(v.x >= Left() && v.x <= Right() && v.y >= Top() && v.y <= Bottom())
			return true;

		return false;
	}

	inline bool Contains(const float x, const float y) const
	{
		return Contains(fvec2(x, y));
	}

	virtual void Draw()
	{
		//Todo change to not this
		float top = Top(), bottom = Bottom(), left = Left(), right = Right();
		glLineWidth(2);
		{
			Shader::Uniform("texture_draw", 0);
			glmesh::Draw i(GL_LINES, 48, *sVFT, *sIBuffer);

			i.Attrib(fvec3(left, 0, top));
			i.Attrib(fvec3(right, 0, top));
			i.Attrib(fvec3(right, 0, top));
			i.Attrib(fvec3(right, 0, bottom));
			i.Attrib(fvec3(right, 0, bottom));
			i.Attrib(fvec3(left, 0, bottom));
			i.Attrib(fvec3(left, 0, bottom));
			i.Attrib(fvec3(left, 0, top));

			i.Attrib(fvec3(left, 11, top));
			i.Attrib(fvec3(right, 11, top));
			i.Attrib(fvec3(right, 11, top));
			i.Attrib(fvec3(right, 11, bottom));
			i.Attrib(fvec3(right, 11, bottom));
			i.Attrib(fvec3(left, 11, bottom));
			i.Attrib(fvec3(left, 11, bottom));
			i.Attrib(fvec3(left, 11, top));

			i.Attrib(fvec3(left, 11, top));
			i.Attrib(fvec3(right, 0, top));
			i.Attrib(fvec3(right, 0, top));
			i.Attrib(fvec3(right, 11, bottom));
			i.Attrib(fvec3(right, 11, bottom));
			i.Attrib(fvec3(left, 0, bottom));
			i.Attrib(fvec3(left, 0, bottom));
			i.Attrib(fvec3(left, 11, top));

			i.Attrib(fvec3(right, 11, top));
			i.Attrib(fvec3(left, 0, top));
			i.Attrib(fvec3(left, 0, top));
			i.Attrib(fvec3(left, 11, bottom));
			i.Attrib(fvec3(left, 11, bottom));
			i.Attrib(fvec3(right, 0, bottom));
			i.Attrib(fvec3(right, 0, bottom));
			i.Attrib(fvec3(right, 11, top));

			i.Attrib(fvec3(left, 0, top));
			i.Attrib(fvec3(right, 0, bottom));
			i.Attrib(fvec3(right, 0, top));
			i.Attrib(fvec3(left, 0, bottom));

			i.Attrib(fvec3(left, 11, top));
			i.Attrib(fvec3(right, 11, bottom));
			i.Attrib(fvec3(right, 11, top));
			i.Attrib(fvec3(left, 11, bottom));

			i.Attrib(fvec3(left, 0, top));
			i.Attrib(fvec3(left, 11, top));

			i.Attrib(fvec3(right, 0, top));
			i.Attrib(fvec3(right, 11, top));

			i.Attrib(fvec3(right, 0, bottom));
			i.Attrib(fvec3(right, 11, bottom));

			i.Attrib(fvec3(left, 0, bottom));
			i.Attrib(fvec3(left, 11, bottom));
		}
	}

};

struct CenteredBox
	: public Box
{
	fvec2 Location;

	float Width;
	float Height;

	virtual float Top() const 
	{
		return Location.y - Height / 2.0f;
	}

	virtual float Bottom() const 
	{
		return Location.y + Height / 2.0f;
	}

	virtual float Left() const 
	{
		return Location.x - Width / 2.0f;
	}

	virtual float Right() const 
	{
		return Location.x + Width / 2.0f;
	}

	virtual fvec2 Center() const
	{
		return Location;
	}
};

struct BoundingBox
	: public Box
{
	float T, B, L, R;

	virtual float Top() const 
	{
		return T;
	}

	virtual float Bottom() const 
	{
		return B;
	}

	virtual float Left() const
	{
		return L;
	}

	virtual float Right() const
	{
		return R;
	}
};

struct BoxExtension
	: public Box
{
	enum class Direction
	{
		NORTH,
		SOUTH,
		EAST,
		WEST
	};

	const Box& Parent;
	BoxExtension(const Box& parent)
		: Parent(parent), Amount(10), Dir(Direction::NORTH)
	{

	}

	float Amount;
	Direction Dir;

	virtual bool Intersects(const Box& other) const
	{
		if(Amount == 0)
			return false;

		return Box::Intersects(other);
	}

	virtual float Top() const 
	{
		switch(Dir)
		{
		case Direction::NORTH:
			return Parent.Top() - Amount;
		case Direction::WEST:
		case Direction::EAST:
			return Parent.Top();
		case Direction::SOUTH:
			return Parent.Top() + Amount;
		}
	}

	virtual float Bottom() const 
	{
		switch(Dir)
		{
		case Direction::NORTH:
			return Parent.Bottom() - Amount;
		case Direction::WEST:
		case Direction::EAST:
			return Parent.Bottom();
		case Direction::SOUTH:
			return Parent.Bottom() + Amount;
		}
	}

	virtual float Left() const
	{
		switch(Dir)
		{
		case Direction::EAST:
			return Parent.Left() + Amount;
		case Direction::NORTH:
		case Direction::SOUTH:
			return Parent.Left();
		case Direction::WEST:
			return Parent.Left() - Amount;
		}
	}

	virtual float Right() const
	{
		switch(Dir)
		{
		
		case Direction::WEST:
			return Parent.Right() - Amount;
		case Direction::SOUTH:
		case Direction::NORTH:
			return Parent.Right();
		case Direction::EAST:
			return Parent.Right() + Amount;
		}
	}

};


struct ShadowMap
{
	unsigned Framebuffer;
	unsigned ShadowTexture;

	ShadowMap(unsigned width, unsigned height)
	{
		glGenFramebuffers(1, &Framebuffer);
		dgGenTextures(1, &ShadowTexture);
		glBindTexture(GL_TEXTURE_2D, ShadowTexture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glBindFramebuffer(GL_FRAMEBUFFER, Framebuffer);
		glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, ShadowTexture, 0);
		glDrawBuffer(GL_NONE);
		glReadBuffer(GL_NONE);
		assert(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
	}

	void BindWrite()
	{
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, Framebuffer);
		glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
	}

	void BindRead(unsigned tex)
	{
		glActiveTexture(tex);
		glBindTexture(GL_TEXTURE_2D, ShadowTexture);
	}
};



void RenderMaze(const Mesh&);
RenderPipe* BuildMazeMesh(const Maze& maze);

void DrawQuad(const fvec3& center, const fvec3& up, const fvec3& right, float width, float height)
{
	width /= 2.0f;
	height /= 2.0f;

	glDisable(GL_CULL_FACE);
	glmesh::Draw i(GL_TRIANGLE_STRIP, 4, *sQuadFormat, *sIBuffer);
	i.Attrib(fvec3(center + up * height - right * width));
	i.Attrib(fvec2(0, 0));
	i.Attrib(fvec3(center - up * height - right * width));
	i.Attrib(fvec2(0, 1));
	i.Attrib(fvec3(center + up * height + right * width));
	i.Attrib(fvec2(1, 0));
	i.Attrib(fvec3(center - up * height + right * width));
	i.Attrib(fvec2(1, 1));
	i.Render();
	glEnable(GL_CULL_FACE);
}

void WriteQuad(CpuDataWriter& vertexWriter, CpuDataWriter& indiceWriter, const fvec3& center, const fvec3& up, const fvec3& right, float width, float height);
void WriteQuad(CpuDataWriter& vertexWriter, CpuDataWriter& indiceWriter, const fvec3& center, const fvec3& up, const fvec3& right, const fvec3& face, float width, float height);
void WriteCube(CpuDataWriter& vertexWriter, CpuDataWriter& indiceWriter, const fvec3& center, const fvec3& up, const fvec3& right, float width, float height, float length);
fvec3 MazeSpaceToWorldSpace(const Maze& maze, const float x, const float y, const float height = 0);
ivec3 WorldSpaceToMazeSpace(const Maze& maze, const float x, const float y, const float height = 0);
void HandleKeyPress(GLFWwindow* window, int key, int code, int action, int mod);



static Maze* sCurrentMaze;
static CenteredBox sPlayerBox;
static BoundingBox sCellBox;
static BoxExtension sMoveBoxes[] = { (sCellBox), (sCellBox), (sCellBox), (sCellBox) };
static FlightCamera* sCurrentCamera;
static Shader* sCurrentProgram;
static ivec3 sMazePosition;
static InputState sInput;
static BoundingBox sMazeBox;


const fvec3 cMazeFloorDims(10, 0, 10); //Witdh, Height



struct RenderPipe
{

	vector<function<void()>> mFuncPipe;

	inline void Render()
	{
		for(auto& func : mFuncPipe)
			func();
	}
};




int main(int argc, char* args[])
{
	glfwInit();
	GLFWwindow* window = glfwCreateWindow(1280, 960, "Title", nullptr, nullptr);


	glfwMakeContextCurrent(window);
	
	
	glload::LoadFunctions();
	glfwSwapInterval(1);
	cout << "GL Version " << glload::GetMajorVersion() << "." << glload::GetMinorVersion() << endl;
	sInput.SunPosition = 5.0f * fvec3(45, 100, 45);
	unique_ptr<Maze> tMaze(MazeBuilder::CreateMazeFromFile("maze/1.txt"));
	unique_ptr<FlightCamera> tCamera(new FlightCamera(45.0f, 1.0f, 500.0f, 4.0/3.0));
	unique_ptr<Shader> tShader;
	{
		using namespace glmesh;
		AttributeList attrs;
		attrs.emplace_back(0, 3, VDT_SINGLE_FLOAT, ADT_FLOAT); //Pos
		sVFT.reset(new VertexFormat(attrs));
		sIBuffer.reset(new StreamBuffer(1024*1024));
		attrs.emplace_back(2, 2, VDT_SINGLE_FLOAT, ADT_FLOAT); //Texture
		sQuadFormat.reset(new VertexFormat(attrs));
	}

	try
	{
		tShader.reset(
			ShaderBuilder()
			.AddFile("shader.vertex")
			.AddFile("shader.fragment")
			.SetAttributeLocation("in_position", 0)
			.SetAttributeLocation("in_normal", 1)
			.SetAttributeLocation("in_tex", 2)
			.Create());
	}
	catch(exception& e)
	{
		cout << e.what() << endl;
		cin.get();
		exit(-1);
	}

	ShadowMap shadows(SHADOW_SIZE, SHADOW_SIZE);
	sMoveBoxes[(int)BoxExtension::Direction::NORTH].Dir = BoxExtension::Direction::NORTH;
	sMoveBoxes[(int)BoxExtension::Direction::SOUTH].Dir = BoxExtension::Direction::SOUTH;
	sMoveBoxes[(int)BoxExtension::Direction::EAST].Dir = BoxExtension::Direction::EAST;
	sMoveBoxes[(int)BoxExtension::Direction::WEST].Dir = BoxExtension::Direction::WEST;
	sInput.ZoomLevel = 40;

	CheckError(
		sCurrentMaze = tMaze.get();
		sCurrentCamera = tCamera.get();
		sCurrentProgram = tShader.get();
		tShader->Bind();
	);

	CheckError(
		unique_ptr<RenderPipe> tPipe(BuildMazeMesh(*tMaze));
		sMazePosition = glm::ivec3(tMaze->GetStart().x, 5, tMaze->GetStart().y);
		sInput.FacingDir = fvec3(1, 0, 0);
	);

	tCamera->mPosition = MazeSpaceToWorldSpace(*tMaze, sMazePosition.x, sMazePosition.z, sMazePosition.y);
	tCamera->mView = glm::lookAt<float>(tCamera->mPosition + fvec3(0, 30, 0), tCamera->mPosition + sInput.FacingDir, fvec3(0, 1, 0));
	tCamera->mProjection = glm::perspective<float>(60.0f, 4.0f/3.0f, 1, 200);
	sMazeBox.L = MazeSpaceToWorldSpace(*tMaze, -1, 0).x;
	sMazeBox.R = sMazeBox.L + (tMaze->GetWidth() + 1) * cMazeFloorDims.x;
	sMazeBox.T = MazeSpaceToWorldSpace(*tMaze, 0, -1).z;
	sMazeBox.B = sMazeBox.T  + (tMaze->GetHeight() + 1) * cMazeFloorDims.z;


	glfwSetKeyCallback(window, [](GLFWwindow* window, int key, int scan, int action, int mods) {
		sInput.WasTurning = sInput.IsTurning;
		sInput.WasMoving = sInput.IsMoving;

		switch(action)
		{
		case GLFW_REPEAT:
		case GLFW_PRESS:
				if(mods != 1)
				{
					switch(key)
					{
					case 'W':
						sInput.MoveAmount = 20.0f;
						sInput.IsMoving = true;
						break;
					case 'S':
						sInput.MoveAmount = -20.0f;
						sInput.IsMoving = true;
						break;

					case 'A':
						sInput.TurnAmount = 180.0f;
						sInput.IsTurning = true;
						break;
					case 'D':
						sInput.TurnAmount = -180.0f;
						sInput.IsTurning = true;
						break;
					case GLFW_KEY_UP:
						sInput.ZoomLevel += 5;
						break;
					case GLFW_KEY_DOWN:
						sInput.ZoomLevel -= 5;
						break;
					case GLFW_KEY_LEFT_SHIFT:
						sInput.HoldMoveSun = true;
						break;
					}
				}
				else
				{
					switch(key)
					{
					case 'W':
						sInput.SunPosition.x += 20.0f;
						break;
					case 'S':
						sInput.SunPosition.x -= 20.0f;
						break;
					case 'D':
						sInput.SunPosition.z += 20.0f;
						break;
					case 'A':
						sInput.SunPosition.z -= 20.0f;
						break;
					}
				}
			break;
		case GLFW_RELEASE:
			switch(key)
			{
			case 'W':
			case 'S':
				sInput.IsMoving = false;
				break;
			case 'A':
			case 'D':
				sInput.IsTurning = false;
				break;
			case GLFW_KEY_F1:
				sInput.ToggleDebug = !sInput.ToggleDebug;
				break;
			case GLFW_KEY_F2:
				sInput.ToggleNoClip = !sInput.ToggleNoClip;
				break;
			case GLFW_KEY_F3:
				sInput.ToggleDrawWalk = !sInput.ToggleDrawWalk;
				break;
			case GLFW_KEY_F4:
				sInput.ToggleBigMap = !sInput.ToggleBigMap;
				break;
			case GLFW_KEY_F5:
				sInput.ToggleDebugTexture = !sInput.ToggleDebugTexture;
				break;
			case GLFW_KEY_KP_0:
			case GLFW_KEY_KP_1:
			case GLFW_KEY_KP_2:
			case GLFW_KEY_KP_3:
			case GLFW_KEY_KP_4:
			case GLFW_KEY_KP_5:
			case GLFW_KEY_KP_6:
			case GLFW_KEY_KP_7:
			case GLFW_KEY_KP_8:
			case GLFW_KEY_KP_9:
				MainShadowMode = key - GLFW_KEY_KP_0;
				sInput.DebugTextureId = key - GLFW_KEY_KP_0;
				break;
			}
			break;
		}
	});

	CheckError(
		Shader::
		Uniform("uniform_model", fmat4(1.0f))
		.Uniform("uniform_view", sCurrentCamera->GetView())
		.Uniform("uniform_projection", sCurrentCamera->GetProjection())
		.Uniform("uniform_diffuse_texture", 0)
		.Uniform("uniform_normal_texture", 1)
		.Uniform("uniform_shadow_texture", 2)
		.Uniform("shadowTexelSize", 1.0f/SHADOW_SIZE);
	);

	CheckError(
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);
		glViewport(0, 0, 1280, 960);
	);

	sPlayerBox.Width = 5.2;
	sPlayerBox.Height = 5.2;
	sPlayerBox.Location = fvec2(tCamera->mPosition.x, tCamera->mPosition.z);
	
	
	double timeStep = 0;
	float timeDelta = 0;
	glfwSetTime(0);

	while(!glfwWindowShouldClose(window))
	{	
		glfwPollEvents();
		timeDelta = glfwGetTime() - timeStep;
		timeStep = glfwGetTime();

		if(sInput.IsTurning)
		{
			sInput.FacingDir = glm::normalize(glm::rotateY(sInput.FacingDir, sInput.TurnAmount * timeDelta));
		}

		if(sInput.IsMoving)
		{
			sPlayerBox.Location.x = tCamera->mPosition.x;
			sPlayerBox.Location.y = tCamera->mPosition.z;

			sMazePosition = WorldSpaceToMazeSpace(*sCurrentMaze, sPlayerBox.Location.x, sPlayerBox.Location.y, 0);
			auto center = MazeSpaceToWorldSpace(*sCurrentMaze, sMazePosition.x, sMazePosition.z, 0);
			fvec2 CellCenter(center.x, center.z);

			const float buf = 5;
			const float boxSize = 10;
			sCellBox.T = CellCenter.y - buf;
			sCellBox.B = CellCenter.y + buf;
			sCellBox.L = CellCenter.x - buf;
			sCellBox.R = CellCenter.x + buf;

			auto tile = sCurrentMaze->GetWall(sMazePosition.x, sMazePosition.z - 1);
			switch(tile)
			{
			case Maze::TileType::Floor:
			case Maze::TileType::Start:
			case Maze::TileType::Walk:
				sMoveBoxes[(int)BoxExtension::Direction::NORTH].Amount = 0;
				break;
			default:
				sMoveBoxes[(int)BoxExtension::Direction::NORTH].Amount = boxSize;
				break;
			}

			tile = sCurrentMaze->GetWall(sMazePosition.x, sMazePosition.z + 1);
			switch(tile)
			{
			case Maze::TileType::Floor:
			case Maze::TileType::Start:
			case Maze::TileType::Walk:
				sMoveBoxes[(int)BoxExtension::Direction::SOUTH].Amount = 0;
				break;
			default:
				sMoveBoxes[(int)BoxExtension::Direction::SOUTH].Amount = boxSize;
				break;
			}

			tile = sCurrentMaze->GetWall(sMazePosition.x - 1, sMazePosition.z);
			switch(tile)
			{
			case Maze::TileType::Floor:
			case Maze::TileType::Start:
			case Maze::TileType::Walk:
				sMoveBoxes[(int)BoxExtension::Direction::WEST].Amount = 0;
				break;
			default:
				sMoveBoxes[(int)BoxExtension::Direction::WEST].Amount = boxSize;
				break;
			}

			tile = sCurrentMaze->GetWall(sMazePosition.x + 1, sMazePosition.z);
			switch(tile)
			{
			case Maze::TileType::Floor:
			case Maze::TileType::Start:
			case Maze::TileType::Walk:
				sMoveBoxes[(int)BoxExtension::Direction::EAST].Amount = 0;
				break;
			default:
				sMoveBoxes[(int)BoxExtension::Direction::EAST].Amount = boxSize;
				break;
			}

			fvec2 newPos = fvec2(sInput.FacingDir.x, sInput.FacingDir.z) * sInput.MoveAmount * timeDelta;
			
			sPlayerBox.Location += newPos;
			bool canMove = true;

			for(int i = 0; i < 4; i++)
			{
				if(sMoveBoxes[i].Amount >= 5)
				{
					if(sMoveBoxes[i].Intersects(sPlayerBox))
					{
						canMove = true;
						if(!sInput.ToggleNoClip)
						{
							fvec2 dir = glm::normalize(sMoveBoxes[i].Center() - sCellBox.Center());
							sPlayerBox.Location -= dir * glm::dot(dir, newPos);
						}
					}
				}
			}

			if(canMove || sInput.ToggleNoClip)
			{
				tCamera->mPosition.x = sPlayerBox.Location.x;
				tCamera->mPosition.z = sPlayerBox.Location.y;
			}
		}

		if(sInput.IsMoving || sInput.IsTurning)
		{
			tCamera->mView = glm::lookAt<float>(tCamera->mPosition + fvec3(0, 35, 0) - sInput.FacingDir*35.0f, tCamera->mPosition + sInput.FacingDir, cYUnitVector);
			sInput.SunDirection = -glm::normalize(sInput.SunPosition);

		}

		tShader->Uniform("uniform_model", fmat4(1.0f));
		BoundingBox lightBox;
		fmat4 LightView = glm::lookAt<float>(sInput.SunPosition, fvec3(0), cXUnitVector);
		{
		/*fvec4 left(-75, 0, 0, 1), right(75, 0, 0, 1), up(0, 0, -75, 1), bottom(0, 0, 75, 1);
		left += fvec4(tCamera->mPosition, 0);
		right += fvec4(tCamera->mPosition, 0);
		bottom += fvec4(tCamera->mPosition, 0);
		up += fvec4(tCamera->mPosition, 0);

		left = LightView * left;
		right = LightView * right;
		up = LightView * up;
		bottom = LightView * bottom;

		float minX = glm::min(left.x, right.x, up.x, bottom.x);
		float maxX = glm::max(left.x, right.x, up.x, bottom.x);
		float minY = glm::min(left.y, right.y, up.y, bottom.y);
		float maxY = glm::max(left.y, right.y, up.y, bottom.y);
		float minZ = glm::min(left.z, right.z, up.z, bottom.z);
		float maxZ = glm::max(left.z, right.z, up.z, bottom.z);

		lightBox.L = minX;
		lightBox.R = maxX;
		lightBox.T = minY;
		lightBox.B = maxY;
*/

		//fmat4 WorldOrtho(glm::ortho<float>(sCellBox.Left() - 100, sCellBox.Right() + 100, sCellBox.Top() - 100, sCellBox.Bottom() + 100, -100, 100));
		//fmat4 LightProj = LightView * WorldOrtho * glm::inverse(LightView);
		fmat4 LightProj = glm::ortho<float>(-110,  110,  -110 , 110, glm::length(sInput.SunPosition) - 100, glm::length(sInput.SunPosition) + 100);
		//fmat4 LightProj = glm::ortho<float>(lightBox.Left(), lightBox.Right(), lightBox.Top(), lightBox.Bottom(), minZ, maxZ);
		//fmat4 LightProj = glm::perspective<float>(90.0f, 1/6.0, 20.0, 500.0f);
		//fmat4 LightProj = glm::ortho<float>(lightBox.Left(), lightBox.Right(), lightBox.Top(), lightBox.Bottom(), glm::length(sInput.SunPosition) - 50, glm::length(sInput.SunPosition) + 50);
		fmat4 LightViewProj =  LightProj * LightView;
		tShader->Uniform("uniform_view", LightView);
		tShader->Uniform("uniform_projection", LightProj);
		tShader->Uniform("lightVP", LightViewProj);
		}
		shadows.BindWrite();
		glViewport(0, 0, SHADOW_SIZE, SHADOW_SIZE);
		glClear(GL_DEPTH_BUFFER_BIT);

		tShader->Uniform("mode", (int)DrawModes::DepthPass);
		tPipe->Render();
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		
		glViewport(0, 0, 1280, 960); //Draw Normal Scene
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);




		tShader->Uniform("ambientFactor", 0.3f);
		tShader->Uniform("diffuseFactor", 1.0f);

		tShader->Uniform("diffuseColor", fvec3(1.0, 1.0, 1.0));
		tShader->Uniform("lightDirection", sInput.SunDirection);
		tShader->Uniform("eyePosition", tCamera->mPosition);

		if(sInput.ToggleBigMap)
		{ //Normal Scene
			tShader->Uniform("uniform_view", tCamera->GetView()); //OH JESUS THIS IS UGLY
			tShader->Uniform("uniform_projection", tCamera->GetProjection());

			if(sInput.ToggleDebug)
			{
				Shader::Uniform("mode", (int)DrawModes::ConstColor);
				Shader::Uniform("uniform_model", fmat4(1.0f));
				Shader::Uniform("vertex_color", fvec4(1, 0, 0, 1));
				sCellBox.Draw();
				Shader::Uniform("vertex_color", fvec4(0, 1, 0, 1));
				sPlayerBox.Draw();
				sMazeBox.Draw();
				Shader::Uniform("vertex_color", fvec4(0, 0, 1, 1));
				for(int i = 0; i < 4; i++)
				{
					sMoveBoxes[i].Draw();
				}
			}

			Shader::Uniform("use_shadow", MainShadowMode);
			shadows.BindRead(GL_TEXTURE2);
			Shader::Uniform("mode", (int)DrawModes::TextureDraw);
			tPipe->Render();
			Shader::Uniform("use_shadow", 0);
		}

		if(sInput.ToggleBigMap)
		{
			glViewport(1000, 680, 280, 280); //Draw Minimap Scene
			glEnable(GL_SCISSOR_TEST);
			glScissor(1000, 680, 280, 280);
		}
		else
		{
			glViewport(0, 0, 1280, 960); //Draw minimap as normal scene
		}

		glClear(GL_DEPTH_BUFFER_BIT);
		
		sInput.ZoomLevel = glm::min<float>(sInput.ZoomLevel, glm::max<float>((sMazeBox.Right() - sMazeBox.Left())/2.0f, (sMazeBox.Bottom() - sMazeBox.Top())/2.0f));

		float left = sPlayerBox.Location.x - sInput.ZoomLevel, 
			right = sPlayerBox.Location.x + sInput.ZoomLevel,
			top =  sPlayerBox.Location.y - sInput.ZoomLevel,
			bottom =   sPlayerBox.Location.y + sInput.ZoomLevel;

		fvec2 offset(0);


		if(bottom - top > sMazeBox.Bottom() - sMazeBox.Top())
			offset.y = -sPlayerBox.Location.y;
		else
		{
			if(bottom > sMazeBox.Bottom())
				offset.y += sMazeBox.Bottom() - bottom;
			else
			if(top < sMazeBox.Top())
				offset.y -= top - sMazeBox.Top();
		}

		if(right - left > sMazeBox.Right() - sMazeBox.Left())
			offset.x = -sPlayerBox.Location.x;
		{
			if(right > sMazeBox.Right())
				offset.x += sMazeBox.Right() - right;
			else
			if(left < sMazeBox.Left())
				offset.x -= left - sMazeBox.Left();
		}

		tShader->Uniform("uniform_view", glm::lookAt<float>(fvec3(offset.x, 30, offset.y) + tCamera->mPosition, tCamera->mPosition + fvec3(offset.x, 0, offset.y), cXUnitVector));
		tShader->Uniform("uniform_projection", glm::ortho<float>(-sInput.ZoomLevel, sInput.ZoomLevel, -sInput.ZoomLevel, sInput.ZoomLevel, 0.1f, 500));


		Shader::Uniform("use_shadow", 1);
		Shader::Uniform("mode", (int)DrawModes::TextureDraw);
		tPipe->Render();
		Shader::Uniform("use_shadow", 0);
		
		if(sInput.ToggleDebug)
		{
			Shader::Uniform("mode", (int)DrawModes::ConstColor);
			Shader::Uniform("uniform_model", fmat4(1.0f));
			Shader::Uniform("vertex_color", fvec4(1, 0, 0, 1));
			sCellBox.Draw();
			Shader::Uniform("vertex_color", fvec4(0, 1, 0, 1));
			sPlayerBox.Draw();
			sMazeBox.Draw();
			Shader::Uniform("vertex_color", fvec4(0, 0, 1, 1));
			for(int i = 0; i < 4; i++)
			{
				sMoveBoxes[i].Draw();
			}
			Shader::Uniform("vertex_color", fvec4(1, 1, 0, 1));
			lightBox.Draw();
		}
		glDisable(GL_SCISSOR_TEST);
		

		if(sInput.ToggleDebugTexture)
		{
			glViewport(0, 0, 1280, 960);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, sInput.DebugTextureId >= sTextures.size() ? 0 : sTextures[sInput.DebugTextureId]);

			Shader::Uniform("mode", (int)DrawModes::Quad);
			Shader::Uniform("uniform_projection", glm::ortho<float>(-640, 640, -480, 480, -5, 5));
			Shader::Uniform("uniform_view", glm::lookAt<float>(fvec3(0, 1, 0), fvec3(0, 0, 0), cXUnitVector));
			DrawQuad(fvec3(0), cZUnitVector, cXUnitVector, 1280, 960);
		}
	
		glfwSwapBuffers(window);

	}
	glfwDestroyWindow(window);
	glfwTerminate();

	return 0;
}


//Height = mag in up direction
//Width = mag in right direction
void WriteQuad(CpuDataWriter& vertexWriter, CpuDataWriter& indiceWriter, const fvec3& center, const fvec3& up, const fvec3& right, float width, float height)
{
	WriteQuad(vertexWriter, indiceWriter, center, up, right, glm::cross(right, up), width, height);
}

//Height = mag in up direction
//Width = mag in right direction
//Face = normal direction
void WriteQuad(CpuDataWriter& vertexWriter, CpuDataWriter& indiceWriter, const fvec3& center, const fvec3& up, const fvec3& right, const fvec3& face, float width, float height)
{
	width /= 2.0f;
	height /= 2.0f;

	unsigned tIndiceOffset = vertexWriter.GetNumVerticesWritten();
	vertexWriter.Attrib(center + up*height - right*width); //Top Left
	vertexWriter.Attrib(face);
	vertexWriter.Attrib(0.0f, 0.0f);
	indiceWriter.Attrib(tIndiceOffset);

	vertexWriter.Attrib(center - up*height - right*width); //Bottom Left
	vertexWriter.Attrib(face);
	vertexWriter.Attrib(0.0f, 1.0f);
	indiceWriter.Attrib(tIndiceOffset + 1);

	vertexWriter.Attrib(center - up*height + right*width); //Bottom Right
	vertexWriter.Attrib(face);
	vertexWriter.Attrib(1.0f, 1.0f);
	indiceWriter.Attrib(tIndiceOffset + 2);
	indiceWriter.Attrib(tIndiceOffset + 2);

	vertexWriter.Attrib(center + up*height + right*width); //Top Right
	vertexWriter.Attrib(face);
	vertexWriter.Attrib(1.0f, 0.0f);
	indiceWriter.Attrib(tIndiceOffset + 3);
	indiceWriter.Attrib(tIndiceOffset);
}

//Height = mag in up direction
//Width = mag in right direction
//Length mag in forward (right x up) direction
void WriteCube(CpuDataWriter& vertexWriter, CpuDataWriter& indiceWriter, const fvec3& center, const fvec3& up, const fvec3& right, float width, float height, float length)
{
	float 
		tHalfWidth = width / 2.0f,
		tHalfLength = length / 2.0f,
		tHalfHeight = height / 2.0f;


	fvec3 face = glm::cross(right, up);
	WriteQuad(vertexWriter, indiceWriter, center - right*tHalfWidth, up, face, length, height); //West Face
	WriteQuad(vertexWriter, indiceWriter, center + right*tHalfWidth, up, -face, length, height); //East Face
	WriteQuad(vertexWriter, indiceWriter, center + up*tHalfHeight, face, -right, width, length); //Top Face
	WriteQuad(vertexWriter, indiceWriter, center - up*tHalfHeight, face, right, width, length); //Bottom Face
	WriteQuad(vertexWriter, indiceWriter, center + face*tHalfLength, up, right, width, height); //North Face
	WriteQuad(vertexWriter, indiceWriter, center - face*tHalfLength, up, -right, width, height); //South Face
}

fvec3 MazeSpaceToWorldSpace(const Maze& maze, const float x, const float y, const float height)
{
	return fvec3(cMazeFloorDims.x * (-maze.GetWidth()/2.0f + x + 0.5f), height, cMazeFloorDims.z * (-maze.GetHeight()/2.0f + y + 0.5f));
}

ivec3 WorldSpaceToMazeSpace(const Maze& maze, const float x, const float y, const float height)
{
	return ivec3(glm::round((x / cMazeFloorDims.x) - 0.5f + maze.GetWidth()/2.0f), height, glm::round((y/cMazeFloorDims.z) - 0.5f + maze.GetHeight()/2.0f));
}



RenderPipe* BuildMazeMesh(const Maze& maze)
{
	using namespace glmesh;

	AttributeList attrs;
	attrs.emplace_back(0, 3, VDT_SINGLE_FLOAT, ADT_FLOAT);
	attrs.emplace_back(1, 3, VDT_SINGLE_FLOAT, ADT_FLOAT);
	attrs.emplace_back(2, 2, VDT_SINGLE_FLOAT, ADT_FLOAT);

	VertexFormat tVertexFormat(attrs);
	attrs.clear();

	attrs.emplace_back(0, 1, VDT_UNSIGN_INT, ADT_INTEGER);
	VertexFormat tIndiceFormat(attrs);

	attrs.clear();
	attrs.emplace_back(0, 3, VDT_SINGLE_FLOAT, ADT_FLOAT);
	VertexFormat tVertexOnly(attrs);

	enum
	{
		WallVertexWriter,
		WallIndexWriter,
		FloorVertexWriter,
		FloorIndiceWriter,
		PlayerVertexWriter,
		PlayerIndiceWriter,
	};

	array<CpuDataWriter, 6> tWriters = 
	{ 
		CpuDataWriter(tVertexFormat), CpuDataWriter(tIndiceFormat), //Wall
		CpuDataWriter(tVertexFormat), CpuDataWriter(tIndiceFormat), //Floor
		CpuDataWriter(tVertexFormat), CpuDataWriter(tIndiceFormat), //Player
	};

	CpuDataWriter tWalkWriter(tVertexOnly);

	for(auto x = 0; x < maze.GetWidth(); x++)
	{
		for(auto y = 0; y < maze.GetHeight(); y++)
		{
			auto tTileType = maze.GetWall(x, y);
			switch(tTileType)
			{
			case Maze::TileType::Start:
				tTileType = Maze::TileType::Floor;
				break;
			case Maze::TileType::Walk:
				tTileType = Maze::TileType::Floor;
				break;
			case Maze::TileType::End:
				tTileType = Maze::TileType::Floor;
				WriteCube(tWriters[FloorVertexWriter], tWriters[FloorIndiceWriter], MazeSpaceToWorldSpace(maze, x, y, 5), cYUnitVector, cXUnitVector, 3, 5, 3);
				break;
			}

			switch(tTileType)
			{
			case Maze::TileType::Floor:
				WriteQuad(tWriters[FloorVertexWriter], tWriters[FloorIndiceWriter], MazeSpaceToWorldSpace(maze, x, y, 0), cXUnitVector, cZUnitVector, cYUnitVector, cMazeFloorDims.x, cMazeFloorDims.z);
				break;
			case Maze::TileType::Wall:
				WriteQuad(tWriters[WallVertexWriter], tWriters[WallIndexWriter], MazeSpaceToWorldSpace(maze, x, y, 10), cXUnitVector, cZUnitVector, cYUnitVector, cMazeFloorDims.x, cMazeFloorDims.z);
				break;
			}

			auto tAdjTileType = maze.GetWall(x + 1, y);
			switch(tAdjTileType)
			{
			case Maze::TileType::Start:
			case Maze::TileType::End:
			case Maze::TileType::Walk:
				tAdjTileType = Maze::TileType::Floor;
			}
			
			if(tAdjTileType != tTileType)
			{
				switch(tAdjTileType)
				{
				case Maze::TileType::Floor: //tTileType == Wall
					WriteQuad(tWriters[WallVertexWriter], tWriters[WallIndexWriter], MazeSpaceToWorldSpace(maze, x + .5, y, 5), cYUnitVector, -cZUnitVector, cMazeFloorDims.x, 10);
					break;
				case Maze::TileType::Wall: //tTileType == Floor
					WriteQuad(tWriters[WallVertexWriter], tWriters[WallIndexWriter], MazeSpaceToWorldSpace(maze, x + .5, y, 5), cYUnitVector, cZUnitVector, cMazeFloorDims.x, 10);
					break;
				}
			}

			tAdjTileType = maze.GetWall(x, y + 1);
			switch(tAdjTileType)
			{
			case Maze::TileType::Start:
			case Maze::TileType::End:
			case Maze::TileType::Walk:
				tAdjTileType = Maze::TileType::Floor;
			}
			if(tAdjTileType != tTileType)
			{
				switch(tAdjTileType)
				{
				case Maze::TileType::Floor: //tTileType == Wall
					WriteQuad(tWriters[WallVertexWriter], tWriters[WallIndexWriter], MazeSpaceToWorldSpace(maze, x, y + .5, 5), cYUnitVector, cXUnitVector, cMazeFloorDims.x, 10);
					break;
				case Maze::TileType::Wall: //tTileType == Floor
					WriteQuad(tWriters[WallVertexWriter], tWriters[WallIndexWriter], MazeSpaceToWorldSpace(maze, x, y + .5, 5), cYUnitVector, -cXUnitVector, cMazeFloorDims.x, 10);
					break;
				}
			}
		}
	}


	WriteCube(tWriters[PlayerVertexWriter], tWriters[PlayerIndiceWriter], fvec3(0,0,0), cYUnitVector, cZUnitVector, 5, 8, 5);


	//Change to something not Horrible later.
	{
		Maze::TileType tile;
		int x = 1, y = 1;
		tWalkWriter.Attrib(MazeSpaceToWorldSpace(maze, x, y, 5));
		int dirx, diry;
		if(maze.GetWall(1, 2) == Maze::TileType::Walk)
		{
			dirx = 0;
			diry = -1;
			y = 2;
		}
		else
		if(maze.GetWall(2, 1) == Maze::TileType::Walk)
		{
			dirx = -1;
			diry = 0;
			x = 2;
		}

		do
		{
			tWalkWriter.Attrib(MazeSpaceToWorldSpace(maze, x, y, 5));
		
			for(int i = 0; i < 3; i++)
			{
				float t = dirx;
				dirx = -diry;
				diry = t;

				tile = maze.GetWall(x + dirx, y + diry);
				if(tile == Maze::TileType::Walk || tile == Maze::TileType::End)
				{
					x += dirx;
					y += diry;
					dirx = -dirx;
					diry = -diry;
					break;
				}
			}
			
		} while(tile != Maze::TileType::End); //Maze needs end or bad things
		tWalkWriter.Attrib(MazeSpaceToWorldSpace(maze, x, y, 5));
	}

	array<unsigned, 4> tVAO;
	array<unsigned, 7> tBufferObjs;

	enum 
	{
		WallVAO,
		FloorVAO,
		PlayerVAO,
		WalkVAO
	};

	glGenVertexArrays(tVAO.size(), tVAO.data());
	glGenBuffers(tBufferObjs.size(), tBufferObjs.data());

	for(auto i = 0; i < tBufferObjs.size() - 1; i++)
	{
		tBufferObjs[i] = tWriters[i].TransferToBuffer(GL_ARRAY_BUFFER, GL_STATIC_DRAW, tBufferObjs[i]);
		i++;
		tBufferObjs[i] = tWriters[i].TransferToBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW, tBufferObjs[i]);
	}

	glBindVertexArray(tVAO[WallVAO]); //Walls buffer
	glBindBuffer(GL_ARRAY_BUFFER, tBufferObjs[0]);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, tBufferObjs[1]);
	tVertexFormat.BindAttributes(0);

	glBindVertexArray(tVAO[FloorVAO]); //Floor buffer
	glBindBuffer(GL_ARRAY_BUFFER, tBufferObjs[2]);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, tBufferObjs[3]);
	tVertexFormat.BindAttributes(0);

	glBindVertexArray(tVAO[PlayerVAO]);
	glBindBuffer(GL_ARRAY_BUFFER, tBufferObjs[4]);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, tBufferObjs[5]);
	tVertexFormat.BindAttributes(0);

	tBufferObjs[6] = tWalkWriter.TransferToBuffer(GL_ARRAY_BUFFER, GL_STATIC_DRAW, tBufferObjs[6]);
	glBindVertexArray(tVAO[WalkVAO]);
	glBindBuffer(GL_ARRAY_BUFFER, tBufferObjs[6]);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	tVertexOnly.BindAttributes(0);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	unique_ptr<RenderPipe> tPipe(new RenderPipe());
	auto& v = tPipe->mFuncPipe;
	array<unsigned, 4> tCounts;
	tCounts[WallVAO] = tWriters[WallIndexWriter].GetNumVerticesWritten();
	tCounts[FloorVAO] = tWriters[FloorIndiceWriter].GetNumVerticesWritten();
	tCounts[PlayerVAO] = tWriters[PlayerIndiceWriter].GetNumVerticesWritten();
	tCounts[WalkVAO] = tWalkWriter.GetNumVerticesWritten();

	array<unsigned, 4> tTextureObjs;
	unique_ptr<glimg::ImageSet> tImgSet;

	try
	{
		tImgSet.reset(glimg::loaders::stb::LoadFromFile("wall_diffuse.png"));
		tTextureObjs[0] = glimg::CreateTexture(tImgSet.get(), 0);

		tImgSet.reset(glimg::loaders::stb::LoadFromFile("wall_normal.png"));
		tTextureObjs[1] = glimg::CreateTexture(tImgSet.get(), 0);

		tImgSet.reset(glimg::loaders::stb::LoadFromFile("floor_diffuse.png"));
		tTextureObjs[2] = glimg::CreateTexture(tImgSet.get(), 0);

		tImgSet.reset(glimg::loaders::stb::LoadFromFile("floor_normal.png"));
		tTextureObjs[3] = glimg::CreateTexture(tImgSet.get(), 0);
	}
	catch(exception& e)
	{
		cout << e.what() << endl;
		throw e;
	}

	sTextures.insert(sTextures.end(), tTextureObjs.begin(), tTextureObjs.end());

	v.emplace_back([tTextureObjs, tCounts, tVAO](){ 

		
		//Walls
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, tTextureObjs[0]);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, tTextureObjs[1]);
		glBindVertexArray(tVAO[WallVAO]);
		Shader::Uniform("uniform_model", fmat4(1.0f));
		glDrawElements(GL_TRIANGLES, tCounts[WallVAO], GL_UNSIGNED_INT, 0);

		//Player
		glBindVertexArray(tVAO[PlayerVAO]);
		Shader::Uniform("uniform_model", glm::translate<float>(sPlayerBox.Location.x, 5, sPlayerBox.Location.y) * glm::orientation(sInput.FacingDir, cZUnitVector));
		glDrawElements(GL_TRIANGLES, tCounts[PlayerVAO], GL_UNSIGNED_INT, 0);

		//Floors
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, tTextureObjs[2]);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, tTextureObjs[3]);
		glBindVertexArray(tVAO[FloorVAO]);
		Shader::Uniform("uniform_model", fmat4(1.0f));
		glDrawElements(GL_TRIANGLES, tCounts[FloorVAO], GL_UNSIGNED_INT, 0);

		if(sInput.ToggleDrawWalk)
		{
			glLineWidth(5);
			Shader::Uniform("mode", (int)DrawModes::ConstColor);
			Shader::Uniform("vertex_color", fvec4(1, 1, 1, 1));
			glBindVertexArray(tVAO[WalkVAO]);
			glDrawArrays(GL_LINE_STRIP, 0, tCounts[WalkVAO]);
		}

		glBindVertexArray(0);

	});

	return tPipe.release();
}

