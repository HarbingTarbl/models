#ifndef _SHADER_H_PP
#define _SHADER_H_PP

#include "Common.hpp"

#include <string>
#include <unordered_map>
#include <vector>

using std::string;
using std::unordered_map;
using std::vector;

using glm::value_ptr;

class Shader
{
public:
	static Shader& Uniform(const string& name, const fmat4& v);
	static Shader& Uniform(const string& name, const fmat3& v);
	static Shader& Uniform(const string& name, const fvec4& v);
	static Shader& Uniform(const string& name, const fvec3& v);
	static Shader& Uniform(const string& name, const fvec2& v);
	static Shader& Uniform(const string& name, const float v);
	static Shader& Uniform(const string& name, const int v);
	static int GetLocation(const string& name);

	static Shader* sCurrentShader;

	void Bind();
	void Apply();

	friend class ShaderBuilder;
	inline unsigned GetProgramId() const { return pProgramId; }

	~Shader();
private:
	unordered_map<string, unsigned> pUniformLocations;
	unsigned pProgramId;

	Shader(const unsigned program);
	Shader(const Shader& other);
};

class ShaderBuilder
{
public:
	void Reset();
	ShaderBuilder& AddFile(const string& path);
	ShaderBuilder& AddString(const string& str, const int type);
	Shader* Create();

	ShaderBuilder& SetAttributeLocation(const string& attrName, const int attrLocation);
	int GetAttributeLocation(const string& attrName);

	inline ShaderBuilder()
		: pCurrentProgram(0)
	{
		Reset();
	}

private:
	ShaderBuilder(const ShaderBuilder& e);
	unsigned pCurrentProgram;
};


#endif
