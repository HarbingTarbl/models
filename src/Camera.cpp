#include "Camera.hpp"
#include <functional>
using glm::value_ptr;

SimpleCamera::SimpleCamera()
{
	using namespace glm;
	mProjection = perspective(60.0f, 4.0f/3.0f, 1.0f, 10000.0f);
	mPosition = fvec3(0,0,0);
	mFront = fvec3(0, 0, 1);
	mRight = fvec3(1, 0, 0);
	Update();
}

void SimpleCamera::Update()
{
	using namespace glm;
	auto up = cross(mFront, mRight);
	mView = lookAt(mPosition, mPosition + mFront, up);
}


fquat Orientation::GetOrientation()
{
	return pOrint;
}

float Orientation::GetYaw()
{
	return pYaw;
}

float Orientation::GetRoll()
{
	return pRoll;
}

float Orientation::GetPitch()
{
	return pPitch;
}

void Orientation::update()
{
	if(!pNeedsUpdate)
		return;

	pNeedsNormal++;

	using namespace glm;
	fvec3 angles(radians(fvec3(pPitch, pYaw, pRoll)));

	if(pNeedsNormal > 1) ///TODO Tweak
	{
		pOrint = normalize(fquat(angles) * pOrint);
		pNeedsNormal = 0;
	}
	else
	{
		pOrint = fquat(angles) * pOrint;
	}

	pNeedsUpdate = false;
	pYaw = pRoll = pPitch = 0;
}

Orientation::Orientation()
	: 
	mYaw(
	[&]() { return GetYaw(); },
	[&](const float f) { pNeedsUpdate = true; return pYaw = f; }),
	mPitch(
	[&]() { return GetPitch(); },
	[&](const float f) { pNeedsUpdate = true; return pPitch = f; }),
	mRoll(
	[&]() { return GetRoll() ; },
	[&](const float f) { pNeedsUpdate = true; return pRoll = f; }),
	pNeedsUpdate(true),
	pNeedsNormal(0),
	pYaw(0),
	pRoll(0),
	pPitch(0)
{

}


FlightCamera::FlightCamera(float fov, float zmin, float zmax, float aspect)
	: pZMin(zmin), pZMax(zmax), pAspect(aspect), mPosition(0, 0, 0), pFov(fov)
{
	mProjection = glm::perspective<float>(fov, aspect, zmin, zmax);
	mView = glm::lookAt<float>(mPosition, mPosition + cZUnitVector, cYUnitVector);
	mViewProjection = mProjection * mView;
}

void FlightCamera::Update()
{
	using namespace glm;

	Orientation::update();
	mProjection = perspective<float>(pFov, pAspect, pZMin, pZMax);
	mFacing = rotate(GetOrientation(), cZUnitVector);
	mView = GetRotation() * lookAt<float>(mPosition, mPosition + cZUnitVector, cYUnitVector);
	//mViewProjection = mProjection * mView;
}

const fmat4& FlightCamera::GetProjection()
{
	return (mProjection);
}

const fmat4& FlightCamera::GetView()
{
	return (mView);
}

const fmat4& FlightCamera::GetViewProjection()
{
	return (mViewProjection);
}



