dofile("../glsdk/links.lua")

solution "NotWebGL"
	location "build"
	debugdir "working"
	buildoptions "-std=c++11"
	project "NotWebGL"
		kind "ConsoleApp"
		language "c++"
		libdirs {"lib"}
		includedirs {"include"}
		files {"src/*.cpp", "src/*.hpp"}
		
		UseLibs {"glload", "glimage", "glm", "glmesh", "glutil"}
		
		configurations {"Debug", "Release"}
		objdir "bin/obj"
		targetdir "bin"

		configuration "windows"
			defines "WIN32"
			links {"glu32", "opengl32", "gdi32", "winmm", "user32", "AntTweakBar", "glfw3"}

		configuration "linux"
			links { "boost_filesystem", "boost_system", "GL", "X11", "Xrandr", "GLU", "pthread"}
		configuration "Debug"
			targetsuffix "D"
			defines "_DEBUG"
			flags "Symbols"

		configuration "Release"
			defines "NDEBUG"
			flags {"OptimizeSpeed", "NoFramePointer", "ExtraWarnings", "NoEditAndContinue"}
