#ifndef _MATERIAL_H_PP_
#define _MATERIAL_H_PP_

#include "Common.hpp"

class Texture;

struct Material
{
	Texture* mTexture;
	fvec4 mDiffuseReflect;
	fvec4 mAmbientReflect;
	fvec4 mSpecularReflect;
	float mShininess;
	fvec4 mEmissionColor;
	void Apply() const;
};


#endif