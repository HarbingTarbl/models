#include "MaterialProperties.hpp"

#include <exception>
#include <memory>

#include <glimg/glimg.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/filesystem.hpp>

using namespace std;


TextureRef::TextureRef(const ptree& tree)
{
	try
	{
		loadTexture(tree.begin()->second.get_value("INVALID_TEXTURE.jpg"));
	}
	catch (exception &e)
	{
		cout << "Invalid format -- " << e.what() << endl;
	}
}

void TextureRef::loadTexture(const path& name)
{
	string native(boost::filesystem::canonical(name).make_preferred().string());
	auto f = sLoadedTextures.find(native);
	if (f == sLoadedTextures.end()) //Load new texture
	{

		using namespace glimg;
		try
		{
			unique_ptr<ImageSet> image(loaders::stb::LoadFromFile(native));
			mTextureId = CreateTexture(image.get(), 0);
		}
		catch (exception& e)
		{
			cout << e.what() << endl;
			cin.get();
			exit(-2);
		}
		sLoadedTextures.emplace_hint(f, native, mTextureId);
	}
	else
		mTextureId = f->second;
}

unordered_map<string, GLuint> TextureRef::sLoadedTextures =
		unordered_map<string, GLuint>();

MaterialProperties::MaterialProperties(const ptree& tree)
{
	using glm::value_ptr;
	mDiffuseTexture = tree.get_child("diffuseTexture");
	auto iter = tree.get_child("diffuseReflectance");

	auto func = [](const ptree::value_type& child)
	{
		return child.second.get_value(0.0f);
	};

	transform(iter.begin(), iter.end(), value_ptr(mDiffuseReflect), func);
	iter = tree.get_child("ambientReflectance");
	transform(iter.begin(), iter.end(), value_ptr(mAmbientReflect), func);
	iter = tree.get_child("specularReflectance");
	transform(iter.begin(), iter.end(), value_ptr(mSpecularReflect), func);
	mShininess = tree.get<float>("shininess");
	iter = tree.get_child("emissionColor");
	transform(iter.begin(), iter.end(), value_ptr(mEmissionColor), func);
}
