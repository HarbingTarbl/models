#include "SceneInfo.hpp"
#include "Shader.hpp"

#include <glload/gl_2_1.h>

void Material::Apply() const
{
	Shader::Uniform("uniform_material_diffuseReflectance", mDiffuseReflect);
	Shader::Uniform("uniform_material_ambientReflectance", mAmbientReflect);
	Shader::Uniform("uniform_material_specularReflectance", mSpecularReflect);
	Shader::Uniform("uniform_material_emissionColor", mEmissionColor);
	Shader::Uniform("uniform_material_shininess", mShininess);
}
