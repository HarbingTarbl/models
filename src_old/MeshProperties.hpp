#ifndef _MESH_PROPERTIES_H_PP_
#define _MESH_PROPERTIES_H_PP_

#include <string>
#include <vector>

#include <glload/gl_2_1.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <boost/property_tree/ptree_fwd.hpp>

using std::vector;
using std::string;

using glm::fvec3;
using glm::fvec2;

using boost::property_tree::ptree;

struct Vertex
{
	fvec3 mPosition;
	fvec3 mNormal;
	fvec3 mTangent;
	fvec2 mUV;
};

struct MeshProperties
{
	MeshProperties(const ptree& tree);

	inline GLuint NumVertices() const
	{
		return mVertices.size();
	}

	inline GLuint NumIndices() const
	{
		return mIndices.size();
	}

	inline const float* RawData() const
	{
		return reinterpret_cast<const float*>(&mVertices[0]);
	}

	inline GLuint IndiceBegin() const
	{
		return *mIndices.begin();
	}

	inline GLuint IndiceEnd() const
	{
		return *mIndices.end();
	}

	vector<Vertex> mVertices;
	vector<GLuint> mIndices;
	unsigned mMaterialIndex;
};

#endif
