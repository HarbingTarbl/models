#ifndef _MATERIAL_H_PP
#define _MATERIAL_H_PP

#include "Common.hpp"

#include <vector>
#include <memory>
#include <unordered_map>
#include <string>

#include <boost/noncopyable.hpp>
#include <boost/ptr_container/ptr_vector.hpp>

#include "Texture.hpp"


using std::vector;
using std::shared_ptr;
using std::unordered_map;
using std::string;

using boost::ptr_vector;


struct Material;
struct Vertex;
struct VertexGrouping;
struct Texture;





struct VertexGroup
{
	vector<unsigned> mIndices;
	vector<Vertex> mVertices;
	Material* mMaterial;
};

struct VertexNode
{
	vector<VertexGroup*> mVertexGroups;
	fmat4 mModelMatrix;
};

struct Scene
{
	ptr_vector<Material> mMaterials;
	ptr_vector<Texture> mTextures;
	ptr_vector<VertexGroup> mVertexGroups;
	ptr_vector<VertexNode> mVertexNodes;

	unordered_map<string, Texture*> mTextureNames;
};


struct Maze
{
	unique_ptr<Scene> mScene;
};


#endif
