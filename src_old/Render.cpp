#include "Render.hpp"
#include "SceneInfo.hpp"
#include "Texture.hpp"

#include <glload/gl_3_0.h>
#include <glutil/Debug.h>

#include <iterator>
#include <iostream>
#include <algorithm>

using std::back_inserter;
using std::cout;
using std::endl;
using glm::value_ptr;

Render::Render(const Scene* scene)
	: mScene(scene)
{
	struct _VertexGroup
	{
		const unsigned mStartIndex;
		const Material* mMaterial;
		const unsigned mSize;

		_VertexGroup(const unsigned startX, const Material* mat, const int size)
			: mStartIndex(startX), mMaterial(mat), mSize(size) {}
	};


	vector<unsigned> tIndice;
	vector<Vertex> tVertex;
	vector<_VertexGroup*> tvG;


	//TODO, use instanced rendering and uniform buffer arrays
	unsigned oIndice = 0;
	for(auto& node : scene->mVertexNodes)
	{
		for(auto& mesh : node.mVertexGroups)
		{
			tVertex.reserve(tVertex.size() + mesh->mIndices.size());

			tIndice.insert(tIndice.end(), mesh->mIndices.begin(), mesh->mIndices.end());
			transform(mesh->mVertices.begin(), mesh->mVertices.end(), back_inserter(tVertex),
					[&](const Vertex& v){
				return Vertex(
							fvec3(node.mModelMatrix * fvec4(v.mPosition, 1.0f)),
							fvec3(node.mModelMatrix * fvec4(v.mNormal, 0.0f)),
							fvec3(node.mModelMatrix * fvec4(v.mTangent, 0.0f)),
							v.mUv ); });

			tvG.push_back(new _VertexGroup(oIndice, mesh->mMaterial, mesh->mIndices.size()));
			oIndice += mesh->mIndices.size();
		}
	}

	sort(tvG.begin(), tvG.end(), [](const _VertexGroup* lhs, const _VertexGroup* rhs) -> bool {
		if(lhs->mMaterial->mTexture != rhs->mMaterial->mTexture)
			return lhs->mMaterial->mTexture < rhs->mMaterial->mTexture;
		return lhs->mMaterial < rhs->mMaterial;
	});

	glGenVertexArrays(1, &mVertexArrayId);
	glGenBuffers(1, &mVertexBufferId);
	glGenBuffers(1, &mIndiceBufferId);

	glBindVertexArray(mVertexArrayId);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferId);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndiceBufferId);
	int err = glGetError();
			if(err != GL_NO_ERROR)
				cout << "BufferBind"<<glutil::ErrorCodeToEnumString(err) << endl <<glutil::ErrorCodeToDescription(err) << endl << endl;

	glBufferData(GL_ARRAY_BUFFER, oIndice*sizeof(Vertex), NULL, GL_STATIC_DRAW);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, oIndice*sizeof(unsigned), NULL, GL_STATIC_DRAW);

	 err = glGetError();
			if(err != GL_NO_ERROR)
				cout << "BufferData" << glutil::ErrorCodeToEnumString(err) << endl <<glutil::ErrorCodeToDescription(err) << endl << endl;

	oIndice = 0;
	for(auto& vg:tvG)
	{
		auto begin = tIndice.begin() + vg->mStartIndex;
		auto end = begin + vg->mSize;
		transform(begin, end, begin,
				[=](const unsigned i) { return i + oIndice; });

		glBufferSubData(GL_ARRAY_BUFFER, oIndice * sizeof(Vertex), vg->mSize * sizeof(Vertex), &tVertex[vg->mStartIndex]);
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, oIndice*sizeof(unsigned), vg->mSize*sizeof(unsigned), &tIndice[vg->mStartIndex]);
		oIndice += vg->mSize;
	}

	err = glGetError();
			if(err != GL_NO_ERROR)
				cout <<"SubData"<< glutil::ErrorCodeToEnumString(err) << endl <<glutil::ErrorCodeToDescription(err) << endl << endl;

	//0:Position
	//1:Normal
	//2:Tangent
	//3:Uv ? BiTangent

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);

	glVertexAttribPointer(0, 3, GL_FLOAT, false, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, mPosition)));
	glVertexAttribPointer(1, 3, GL_FLOAT, false, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, mNormal)));
	glVertexAttribPointer(2, 3, GL_FLOAT, false, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, mTangent)));
	glVertexAttribPointer(3, 2, GL_FLOAT, false, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, mUv)));

	mElementsCount = oIndice;

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	//Build render pipe
	mRenderPipe.push_back([this]()->void{glBindVertexArray(mVertexArrayId);});
	const Texture* cTex = nullptr;
	const Material* cMat = nullptr;
	unsigned cIndice = 0;
	unsigned cSize = 0;
	for(auto& vg: tvG)
	{
		if(cTex != vg->mMaterial->mTexture)
		{
			if(cSize != 0)
			{
				mRenderPipe.push_back(
						[cSize, cIndice]()->void
						{
							glDrawElements(GL_TRIANGLES, cSize, GL_UNSIGNED_INT, reinterpret_cast<void*>(cIndice * sizeof(unsigned)));
						});
				cIndice += cSize;
				cSize = 0;
			}

			cTex = vg->mMaterial->mTexture;
			mRenderPipe.push_back([cTex]()->void{glBindTexture(GL_TEXTURE_2D, cTex->mTextureId);});

		}

		if(cMat != vg->mMaterial)
		{
			if(cSize != 0)
			{
				mRenderPipe.push_back(
						[cSize, cIndice]()->void
						{
							glDrawElements(GL_TRIANGLES, cSize, GL_UNSIGNED_INT, reinterpret_cast<void*>(cIndice * sizeof(unsigned)));
						});
				cIndice += cSize;
				cSize = 0;
			}

			cMat = vg->mMaterial;
			mRenderPipe.push_back([cMat]()->void{cMat->Apply();});
		}

		cSize += vg->mSize;
		delete vg;
	}

	if(cSize != 0)
	{
		mRenderPipe.push_back(
				[cSize, cIndice]()->void
				{
					glDrawElements(GL_TRIANGLES, cSize, GL_UNSIGNED_INT, reinterpret_cast<void*>(cIndice * sizeof(unsigned)));
				});
	}

	mRenderPipe.push_back([this]()->void{glBindVertexArray(0);});
};


void Render::Draw()
{
	for(auto& f:mRenderPipe)
		f();
}

