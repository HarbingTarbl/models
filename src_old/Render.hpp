#ifndef _RENDERER_H_PP_
#define _RENDERER_H_PP_

#include "Shader.hpp"
#include "Common.hpp"

#include <glload/gl_3_0.h>

#include <vector>
#include <unordered_map>
#include <string>
#include <functional>

using std::vector;
using std::unordered_map;
using std::string;

class Scene;
struct MaterialGroup;
struct RenderGroup;

///TODO
//For now just stick em all in one huge vertex array. Separate them out later if needed.
struct Render
{
	unsigned mVertexBufferId;
	unsigned mIndiceBufferId;
	unsigned mVertexArrayId;

	unsigned mElementsCount;

	typedef std::function<void()> RenderPipeFunc;
	vector<RenderPipeFunc> mRenderPipe;
	const Scene* mScene;

	Render(const Scene* scene);
	void Draw();

};



#endif
