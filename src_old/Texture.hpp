#ifndef _TEXTURE_H_PP_
#define _TEXTURE_H_PP_

#include <string>

using std::string;

struct Texture
{
	unsigned mTextureId;
	unsigned mActiveSlot;
	string mName;
};

#endif
