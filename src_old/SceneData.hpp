#ifndef _SCENE_DATA_H_PP_
#define _SCENE_DATA_H_PP_

#include <string>

#include <glload/gl_2_1.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <boost/filesystem/path.hpp>

#include "MeshProperties.hpp"
#include "MaterialProperties.hpp"

using boost::filesystem::path;
using std::vector;
using std::string;
using glm::fmat4;


struct SceneData
{

	SceneData(const path& file, const fmat4& pre_transform);
	~SceneData();

	inline GLuint NumVertices() const
	{
		return pNumVertices;
	}

	inline GLuint NumIndices() const
	{
		return pNumIndices;
	}

	inline void Bind() const
	{
		glBindVertexArray(pMeshVAO);
	}

	string mName;
	vector<MaterialProperties> mMaterials;
	vector<MeshProperties> mMeshes;
	fmat4 mTransform;

private:
	GLuint pMeshArrayBuffer;
	GLuint pMeshElementBuffer;
	GLuint pMeshVAO;
	GLuint pNumIndices;
	GLuint pNumVertices;
	GLuint pMaterialTexture;


	void preTransform();
	void buildBuffers();
	void buildNodes();
};


#endif
