#ifndef _MATERIAL_PROPERTIES_H_PP_
#define _MATERIAL_PROPERTIES_H_PP_

#include <string>
#include <algorithm>
#include <unordered_map>


#include <glload/gl_2_1.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <boost/property_tree/ptree_fwd.hpp>
#include <boost/filesystem/path.hpp>

using std::transform;
using std::string;
using std::unordered_map;

using glm::fvec4;

using boost::property_tree::ptree;
using boost::filesystem::path;

enum AttributeLocations
{
	POSITION = 0,
	NORMAL = 1,
	TANGENT = 2,
	UV = 3
};

struct AttributeSpec
{
	AttributeSpec(const string& name, const GLuint location)
		: mName(name), mLocation(location) {}

	string mName;
	GLuint mLocation;
};

struct TextureRef
{
	inline TextureRef(const path& filename)
	{
		loadTexture(filename);
	}


	inline TextureRef()
		: mTextureId(0) {}

	TextureRef(const ptree& tree);

	GLuint mTextureId;

private:
	void loadTexture(const path& name);

	static unordered_map<string, GLuint> sLoadedTextures;

};




struct MaterialProperties
{
	inline MaterialProperties(const TextureRef& diffuseTexture,
			fvec4 diffuseReflect, fvec4 ambientReflect,
			fvec4 specularReflect, float shiny, fvec4 emissiveColor)
		: mDiffuseTexture(diffuseTexture), mDiffuseReflect(diffuseReflect),
		  mAmbientReflect(ambientReflect), mSpecularReflect(specularReflect),
		  mShininess(shiny), mEmissionColor(emissiveColor) {}

	MaterialProperties(const ptree& tree);

	TextureRef mDiffuseTexture;
	fvec4 mDiffuseReflect;
	fvec4 mAmbientReflect;
	fvec4 mSpecularReflect;
	float mShininess;
	fvec4 mEmissionColor;
};

#endif
