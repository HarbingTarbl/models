#include "SceneData.hpp"

#include <exception>
#include <iostream>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/filesystem.hpp>

SceneData::SceneData(const path& file, const fmat4& pre_transform)
		: pNumVertices(0), pNumIndices(0)
{
	using namespace std;
	try
	{
		using glm::value_ptr;
		ptree tree;
		boost::property_tree::read_json(canonical(file).string(), tree);
		boost::filesystem::current_path(file.parent_path());

		mName = tree.get("name", "INVALID_NAME");
		auto iter = tree.get_child("materials");
		mMaterials.reserve(iter.size());
		transform(iter.begin(), iter.end(), back_inserter(mMaterials),
				[](const ptree::value_type& v)
				{	return v.second;});

		iter = tree.get_child("meshes");
		mMeshes.reserve(iter.size());
		transform(iter.begin(), iter.end(), back_inserter(mMeshes),
				[](const ptree::value_type& v)
				{	return v.second;});

		auto nodes = tree.get_child("nodes");
		auto matrix = nodes.begin()->second.get_child("modelMatrix");
		transform(matrix.begin(), matrix.end(), value_ptr(mTransform),
				[](const ptree::value_type& v)
				{	return v.second.get_value(0.0f);});

		mTransform = pre_transform * mTransform;
		preTransform();
		buildBuffers();

	}
	catch (std::exception& e)
	{
		cout << e.what() << endl;
	}
}

SceneData::~SceneData()
{

}

void SceneData::preTransform()
{
	for (auto& mesh : mMeshes)
	{
		transform(mesh.mIndices.begin(), mesh.mIndices.end(),
				mesh.mIndices.begin(),
				[this] (const unsigned i)
				{	return i + pNumIndices;});

		for (auto& vertex : mesh.mVertices)
		{
			vertex.mPosition = fvec3(
					mTransform * fvec4(vertex.mPosition, 1.0f));
			vertex.mNormal = fvec3(mTransform * fvec4(vertex.mNormal, 0.0f));
			vertex.mTangent = fvec3(mTransform * fvec4(vertex.mTangent, 0.0f));
		}

		pNumVertices += mesh.NumVertices();
		pNumIndices += mesh.NumIndices();
	}
}

void SceneData::buildBuffers()
{
	glGenBuffers(1, &pMeshArrayBuffer);
	glGenBuffers(1, &pMeshElementBuffer);
	glGenVertexArrays(1, &pMeshVAO);

	glBindBuffer(GL_ARRAY_BUFFER, pMeshArrayBuffer);
	glBufferData(GL_ARRAY_BUFFER, pNumVertices * sizeof(Vertex), NULL,
			GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pMeshElementBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, pNumIndices * sizeof(GLuint), NULL,
			GL_STATIC_DRAW);

	GLuint indiceOffset(0);
	GLuint vertexOffset(0);

	for (auto& mesh : mMeshes)
	{
		glBufferSubData(GL_ARRAY_BUFFER, vertexOffset * sizeof(Vertex),
				mesh.NumVertices() * sizeof(Vertex), mesh.RawData());
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, indiceOffset * sizeof(GLuint),
				mesh.NumIndices() * sizeof(GLuint), &mesh.mIndices[0]);

		indiceOffset += mesh.NumIndices();
		vertexOffset += mesh.NumVertices();
	}

	glBindVertexArray(pMeshVAO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pMeshElementBuffer);
	glEnableVertexAttribArray(AttributeLocations::NORMAL);
	glEnableVertexAttribArray(AttributeLocations::TANGENT);
	glEnableVertexAttribArray(AttributeLocations::POSITION);
	glEnableVertexAttribArray(AttributeLocations::UV);

	glVertexAttribPointer(AttributeLocations::POSITION, 3, GL_FLOAT, false,
			sizeof(Vertex),
			reinterpret_cast<void*>(offsetof(Vertex, mPosition)));
	glVertexAttribPointer(AttributeLocations::NORMAL, 3, GL_FLOAT, false,
			sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, mNormal)));
	glVertexAttribPointer(AttributeLocations::TANGENT, 3, GL_FLOAT, false,
			sizeof(Vertex),
			reinterpret_cast<void*>(offsetof(Vertex, mTangent)));
	glVertexAttribPointer(AttributeLocations::UV, 2, GL_FLOAT, false,
			sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, mUV)));

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void SceneData::buildNodes()
{

}

