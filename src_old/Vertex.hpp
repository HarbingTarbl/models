#ifndef _VERTEX_H_PP_
#define _VERTEX_H_PP_

#include "Common.hpp"

struct VertexPositionNormalTexture
{
	inline VertexPositionNormalTexture(const fvec3& pos, const fvec3& norm, const fvec3& tang, const fvec2& uv)
		: mPosition(pos), mNormal(norm), mUv(uv) {}

	inline VertexPositionNormalTexture()
		:mPosition(0), mNormal(0), mUv(0)
	{}

	fvec3 mPosition;
	fvec3 mNormal;
	fvec2 mUv;
};

#endif