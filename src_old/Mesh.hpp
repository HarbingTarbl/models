#ifndef _MESH_H_PP_
#define _MESH_H_PP_

#include "Common.hpp"
#include <vector>

using std::vector;

class MeshData
{
public:
	enum class ElementType
	{
		Vertex,
		Normal,
		Texture,
		Custom,
	};

	virtual const unsigned GetElementCount() = 0;
	virtual const float* GetElementData(const ElementType type, const unsigned index) = 0;
	virtual const unsigned GetElementSize(const ElementType type) = 0;
	virtual const bool HasElement(const ElementType type) = 0;

	virtual ~MeshData() {}
};

class VertexNormalTextureMeshData
	: public MeshData
{
private:
	vector<fvec3> pPositions;
	vector<fvec3> pNormals;
	vector<fvec2> pTextures;

public:
	const unsigned GetElementCount();
	const float* GetElementData(const MeshData::ElementType type, const unsigned index);
	const unsigned GetElementSize(const  MeshData::ElementType type);

	inline void Normal(const fvec3& value) { return Normal(value.x, value.y, value.z); }
	void Normal(const float x, const float y, const float z);

	inline void Position(const fvec3& value) { return Position(value.x, value.y, value.z); }
	void Position(const float x, const float y, const float z);

	inline void Texture(const fvec2& value) { return Texture(value.x, value.y); }
	void Texture(const float x, const float y);
};




#endif