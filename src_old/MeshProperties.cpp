#include "MeshProperties.hpp"

#include <boost/property_tree/ptree.hpp>
#include <algorithm>


MeshProperties::MeshProperties(const ptree& tree)
{
	using glm::value_ptr;


	auto indice = tree.get_child("indices");
	mIndices.resize(indice.size());
	transform(indice.begin(), indice.end(), mIndices.begin(),
			[](const ptree::value_type& v) { return v.second.get_value<float>(); });

	mMaterialIndex = tree.get<unsigned>("materialIndex");

	auto vertIter = tree.get_child("vertexPositions").begin();
	auto uvIter =
			tree.get_child("vertexTexCoordinates").begin()->second.begin();
	auto normalIter = tree.get_child("vertexNormals").begin();
	auto tanIter = tree.get_child("vertexTangents").begin();

	auto numVerts = tree.get_child("vertexPositions").size();
	assert(numVerts % 3 == 0);
	mVertices.resize(numVerts / 3);

	float* rawVertex = reinterpret_cast<float*>(&mVertices[0]);

	for (auto x = 0; x < mVertices.size(); x++)
	{
		*rawVertex++ = vertIter++->second.get_value(0.0f);
		*rawVertex++ = vertIter++->second.get_value(0.0f);
		*rawVertex++ = vertIter++->second.get_value(0.0f);

		*rawVertex++ = normalIter++->second.get_value(0.0f);
		*rawVertex++ = normalIter++->second.get_value(0.0f);
		*rawVertex++ = normalIter++->second.get_value(0.0f);

		*rawVertex++ = tanIter++->second.get_value(0.0f);
		*rawVertex++ = tanIter++->second.get_value(0.0f);
		*rawVertex++ = tanIter++->second.get_value(0.0f);

		*rawVertex++ = uvIter++->second.get_value(0.0f);
		*rawVertex++ = uvIter++->second.get_value(0.0f);
	}
}
