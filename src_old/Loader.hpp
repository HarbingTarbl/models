#ifndef _LOADER_H_PP_
#define _LOADER_H_PP_

#include <memory>
#include <string>

#include <boost/property_tree/ptree_fwd.hpp>

using std::unique_ptr;
using std::string;

using boost::property_tree::ptree;

struct Material;
struct Vertex;
struct Texture;
struct Scene;
struct VertexGroup;
struct VertexNode;

class SceneBuilder
{
public:
	SceneBuilder();
	~SceneBuilder();


	void ImportJsonScene(const string& filepath);

	inline Scene* release()
	{
		return pScene.release();
	}

private:
	unique_ptr<Scene> pScene;

	Material* loadMaterial(const ptree& tree);
	Texture* loadTexture(const ptree& tree);
	VertexGroup* loadGroup(const ptree& tree);

	VertexNode* loadNode(const ptree& tree);
};

class ObjLoader
{
	static void LoadObjFile(const string& filepath);
};


#endif
