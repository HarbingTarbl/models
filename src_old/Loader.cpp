#include "Loader.hpp"
#include "Texture.hpp"
#include "SceneInfo.hpp"

#include <glimg/glimg.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/filesystem.hpp>

#include <iostream>

using glm::value_ptr;
using std::unique_ptr;
using std::transform;
using boost::ptr_vector;

SceneBuilder::SceneBuilder()
	: pScene(new Scene())
{

}

SceneBuilder::~SceneBuilder()
{

}

void SceneBuilder::ImportJsonScene(const string& filepath)
{
	using boost::filesystem::path;
	using boost::property_tree::json_parser::read_json;

	path abspath(boost::filesystem::canonical(filepath));
	boost::filesystem::current_path(abspath.parent_path());

	ptree tree;
	read_json(abspath.string(), tree);

	auto materials = tree.get_child("materials");
	auto groups = tree.get_child("meshes");
	auto nodes = tree.get_child("nodes");


	for(auto& mat:materials)
	{
		pScene->mMaterials.push_back(loadMaterial(mat.second));
	}

	for(auto& group:groups)
	{
		pScene->mVertexGroups.push_back(loadGroup(group.second));
	}

	for(auto& node:nodes)
	{
		pScene->mVertexNodes.push_back(loadNode(node.second));
	}

}

Texture* SceneBuilder::loadTexture(const ptree& tree)
{
	if(tree.size() == 0)
		return pScene->mTextureNames.begin()->second;

	auto name =
			boost::filesystem::canonical(tree.begin()->second.get_value("INVALID_TEXTURE")).string();

	auto result = pScene->mTextureNames.find(name);

	if(result == pScene->mTextureNames.end()) //Not Loaded
	{
		unique_ptr<Texture> tex(new Texture());
		tex->mName = name;
		tex->mActiveSlot = 0; //TOD
		using namespace glimg;
		unique_ptr<ImageSet> img(loaders::stb::LoadFromFile(name));
		tex->mTextureId = CreateTexture(img.get(), 0);
		pScene->mTextureNames.emplace_hint(result, name, tex.get());
		return tex.release();
	}
	else
	{
		return result->second;
	}
}

Material* SceneBuilder::loadMaterial(const ptree& tree)
{
	auto texture = tree.get_child("diffuseTexture");
	auto diffuse = tree.get_child("diffuseReflectance");
	auto ambient = tree.get_child("ambientReflectance");
	auto specular = tree.get_child("specularReflectance");
	auto shiny = tree.get_child("shininess");
	auto emission = tree.get_child("emissionColor");

	unique_ptr<Material> material(new Material());
	auto read_float = [](const ptree::value_type& v) { return v.second.get_value<float>(); };
	transform(diffuse.begin(), diffuse.end(), value_ptr(material->mDiffuseReflect), read_float);
	transform(ambient.begin(), ambient.end(), value_ptr(material->mAmbientReflect), read_float);
	transform(specular.begin(), specular.end(), value_ptr(material->mSpecularReflect), read_float);
	transform(emission.begin(), emission.end(), value_ptr(material->mEmissionColor), read_float);
	transform(shiny.begin(), shiny.end(), &(material->mShininess), read_float);
	material->mTexture = loadTexture(texture);

	return material.release();
}

VertexGroup* SceneBuilder::loadGroup(const ptree& tree)
{
	auto pos = tree.get_child("vertexPositions");
	auto norm = tree.get_child("vertexNormals");
	auto tang = tree.get_child("vertexTangents");
	auto uv = tree.get_child("vertexTexCoordinates");
	auto indices = tree.get_child("indices");

	unique_ptr<VertexGroup> group(new VertexGroup());
	auto read_int = [](const ptree::value_type& v){ return v.second.get_value<unsigned>(); };
	unsigned vertices = pos.size() / 3;

	group->mVertices.resize(vertices);
	group->mIndices.resize(vertices);

	transform(indices.begin(), indices.end(), group->mIndices.begin(), read_int);

	auto posIter = pos.begin();
	auto normIter = norm.begin();
	auto tangIter = tang.begin();
	auto uvIter = uv.begin(); //Lets ignore multi channel UV's for now.
	if(uv.size() == 0)
	{
		uvIter = uv.end();
	}
	else
	{
		uvIter = uvIter->second.begin();
	}
	auto raw = reinterpret_cast<float*>(&group->mVertices[0]);

	while(vertices--)
	{
		*raw++ = posIter++->second.get_value<float>();
		*raw++ = posIter++->second.get_value<float>();
		*raw++ = posIter++->second.get_value<float>();

		if(normIter != norm.end())
		{
			*raw++ = normIter++->second.get_value<float>();
			*raw++ = normIter++->second.get_value<float>();
			*raw++ = normIter++->second.get_value<float>();
		}
		else
		{
			*raw++ = 1;
			*raw++ = 1;
			*raw++ = 1;
		}

		if(tangIter != tang.end())
		{
			*raw++ = tangIter++->second.get_value<float>();
			*raw++ = tangIter++->second.get_value<float>();
			*raw++ = tangIter++->second.get_value<float>();
		}
		else
		{
			*raw++ = 1;
			*raw++ = 1;
			*raw++ = 1;
		}

		if(uvIter != uv.end())
		{
			*raw++ = uvIter++->second.get_value<float>();
			*raw++ = uvIter++->second.get_value<float>();
		}
		else
		{
			*raw++ = 0;
			*raw++ = 0;
		}
	}

	auto matIndex = tree.get<unsigned>("materialIndex");
	group->mMaterial = &pScene->mMaterials[matIndex];

	return group.release();
}

VertexNode* SceneBuilder::loadNode(const ptree& tree)
{
	auto modelMat = tree.get_child("modelMatrix");
	auto meshIndi = tree.get_child("meshIndices");
	unique_ptr<VertexNode> node(new VertexNode());
	auto read_float = [](const ptree::value_type& v) { return v.second.get_value<float>(); };
	transform(modelMat.begin(), modelMat.end(), value_ptr(node->mModelMatrix), read_float);

	node->mVertexGroups.resize(meshIndi.size());
	transform(meshIndi.begin(), meshIndi.end(), node->mVertexGroups.begin(),
			[this](const ptree::value_type& v)
			{
				return &pScene->mVertexGroups[v.second.get_value<unsigned>()];
			});

	return node.release();
}



void ObjLoader::LoadObjFile(const string& filepath)
{
	//Quick and dirty
	



}
